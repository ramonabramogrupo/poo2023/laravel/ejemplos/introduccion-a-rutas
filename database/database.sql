USE aplicacion1;

CREATE TABLE noticias (
    id int AUTO_INCREMENT, titulo varchar(255), texto varchar(255), PRIMARY KEY (id)
);

INSERT INTO
    noticias (id, titulo, texto)
VALUES (
        1, 'Noticia 1', 'Texto de la noticia 1'
    ),
    (
        2, 'Noticia 2', 'Texto de la noticia 2'
    );