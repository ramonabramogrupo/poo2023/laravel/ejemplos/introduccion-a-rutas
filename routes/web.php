<?php

use App\Http\Controllers\PrincipalController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

// ruta de tipo get
// /mensaje => "hola clase"
Route::get('/mensaje', function () {
    return "hola clase";
});

// ruta de tipo get
// /menu => retorna un menu
Route::get('/menu', function () {
    $menu = "<ul>";
    $menu .= "<li>item 1</li>";
    $menu .= "<li>item 2</li>";
    $menu .= "<li>item 3</li>";
    $menu .= "</ul>";
    return $menu;
});

// ruta de tipo get
// llama a una vista
// /inicio => renderiza la vista inicio
Route::get('/inicio', function () {
    // logica de control
    return view('inicio');
});


// ruta para cargar vista
// /contacto => renderiza la vista contacto
Route::view('/contacto', 'contacto');

// ruta de tipo get
// /listado => renderiza la vista listado
// donde me muestra los dias de la semana
Route::get('/listado', function () {
    // logica de control

    return view('listado', [
        'dias' => ['lunes', 'martes', 'miercoles', 'jueves', 'viernes', 'sabado', 'domingo']
    ]);
});

// ruta de tipo view
// /colores => renderiza la vista colores

Route::view('/colores', 'colores', [
    'colores' => ['red', 'blue', 'green', 'yellow']
]);


// ruta de tipo get
// / => llamo a la accion index del controlador Principal
// y en la accion index del controlador renderizo la vista menu

Route::get('/', [
    PrincipalController::class, // controlador
    'index' // accion
]);


// Ruta de tipo get
// /noticias ==> listar las noticias de la tabla noticias

Route::get('/noticias', function () {
    // sacar todas las noticias
    // select * from noticias
    // no tenemos modelo lo voy a realizar con la clase DB
    $noticias = DB::table('noticias')->get();
    return $noticias;
    //dd($noticias);
});

// ruta de tipo get
// /noticias1 ==> llamar a la vista noticias. En la vista noticias me 
//                muestra todas las noticias de la tabla noticias

Route::get('/noticias1', function () {
    // sacar todas las noticias
    // select * from noticias
    // no tenemos modelo lo voy a realizar con la clase DB
    $noticias = DB::table('noticias')->get();

    // renderizo la vista y le mando las noticias
    return view('noticias', [
        'noticias' => $noticias
    ]);
});


// ruta de tipo get
// /noticias2 ==> llamar a la accion noticias del controlador Principal
//                en la accion noticias saco todas las noticias de 
//                la tabla noticias y llama a la vista noticias

Route::get('/noticias2', [
    PrincipalController::class, // controlador
    'noticias' // accion
]);
