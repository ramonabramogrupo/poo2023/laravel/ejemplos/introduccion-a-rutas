<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PrincipalController extends Controller
{
    public function index()
    {
        //logica de control

        // renderizo la vista menu
        return view('principal.menu');
    }

    public function noticias()
    {
        // logica de control
        $noticias = DB::table('noticias')->get();
        return view('noticias', [
            'noticias' => $noticias
        ]);
    }
}
