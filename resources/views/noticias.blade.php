<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h1>Noticias</h1>
    @include('menu')
    <ul style="list-style: none">
        @foreach ($noticias as $noticia)
            <li>
                <ul style="list-style: none;border: 1px solid;width:200px;margin:10px;padding:5px">
                    @foreach ($noticia as $nombre => $valor)
                        <li>
                            {{ $nombre }}:{{ $valor }}
                        </li>
                    @endforeach
                </ul>
            </li>
        @endforeach
    </ul>
</body>

</html>
